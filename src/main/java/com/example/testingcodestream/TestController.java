package com.example.testingcodestream;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {

	@GetMapping("/code-stream")
	public String testingMethod() {
		log.info("Inside testing Method");
		return "Connected..!!";
	}
}
