package com.example.testingcodestream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingCodeStreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingCodeStreamApplication.class, args);
	}

}
